# TF Bucket Maker

A simple project use Terraform to build S3 buckets in AWS.

## Prereqs:
- Define AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY in CI/CD Variables
- Create S3 bucket and update backend/key/region in backend.tf

This project uses a number of techniques in build:

1. tflint
1. tfsec
1. checkov
1. terraform-compliance
1. opa (Open Policy Agent)