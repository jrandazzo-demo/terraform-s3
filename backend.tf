terraform {
  backend "s3" {
    bucket = "terraform-s3-example"
    key    = "tf-s3-example/terraform"
    region = "us-east-2"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.50.0"
    }
  }
}
