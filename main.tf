module "bucket-1" {
  source             = "./modules/tf-state-bucket"
  bucket_name        = "bucket-1"
  bucket_environment = "dev"
  cost_center        = "1236"
  project            = "tf-bucket-maker"
}

module "bucket-2" {
  source             = "./modules/tf-state-bucket"
  bucket_name        = "bucket-2"
  bucket_environment = "dev"
  cost_center        = "1235"
  owner              = "James Twitch"
  project            = "tf-bucket-maker"
}

module "bucket-3" {
  source             = "./modules/tf-state-bucket"
  bucket_name        = "bucket-3"
  bucket_environment = "dev"
  cost_center        = "1239"
  owner              = "Gary Smith"
  project            = "tf-bucket-maker"
}

module "bucket-4" {
  source             = "./modules/tf-state-bucket"
  bucket_name        = "bucket-4"
  bucket_environment = "prod"
  cost_center        = "1239"
  owner              = "Gary Smith"
  project            = "tf-bucket-maker"
}
